import sys
import cv2
from matplotlib import pyplot as plt
import numpy as np
import math
import random
import time

# ZPO FIT VUT 2015/2016
# xpriby06 - Pribylova Katerina
# xobluk00 - Oblukova Alena
# xsemml01 - Semmler Jiri

class Histogram(object):
    originalMatrix = None
    originalHist = None
    percentage = 0
    matrixWidth = 0
    matrixHeight = 0
    pixelCount = 0
    methodTime = 0.0
    histTime = 0.0
    pixelCountOriginal = 0

    def get_matrix_info(self):
        self.matrixHeight = self.originalMatrix.__len__()
        self.matrixWidth = self.originalMatrix[0].__len__()
        self.pixelCount = (self.matrixHeight * self.matrixWidth) * (self.percentage / 100.0)
        self.pixelCountOriginal = (self.matrixHeight * self.matrixWidth)

    def submethod_patterning(self):
        """
        Vyrobi se maska, ktera se 100krat zkopiruje. Velikost masky se prizbusobi velikosti obrazku,
        vzdy to tam nacpu 10x
        Do kazde masky dam tolik pixelu, kolik mi bude vychazet
        """
        ret = []

        mask_width = int(math.floor(self.matrixWidth / 10))
        mask_height = int(math.floor(self.matrixHeight / 10))

        # musim zaokrouhlit nahoru, u posledniho priplacnuti vezmu min prvku ;)
        pixel_in_one_mask = int(math.ceil(self.pixelCount / 100))

        while pixel_in_one_mask > 0:
            x = random.randint(0, mask_height - 1)
            y = random.randint(0, mask_width - 1)

            for i in range(10):
                # j = 0
                for j in range(10):
                    ret.append(self.originalMatrix[x + i * mask_height][y + j * mask_width])
            pixel_in_one_mask -= 1

        cut_array = ret[:int(self.pixelCount)]
        return np.asarray(cut_array, dtype=np.uint8)

    def submethod_n_neighbourhood(self):
        """
        najdu nahodne x pixelu a k tem vezmu jejich 8mi okoli
        """
        ret = []
        random_numbers = int(math.ceil(self.pixelCount / 9))  # 8mi okoli + ten vybrany prvek
        while random_numbers > 0:
            row = random.randint(1, self.matrixHeight - 2)
            # nahodne cislo pro osu x vynechavajici krajni body (at nemusim hlidat podminky u vyberu okoli....)
            cols = random.randint(1, self.matrixWidth - 2)

            # beru vsechny body
            ret.append(self.originalMatrix[row - 1][cols - 1])
            ret.append(self.originalMatrix[row - 1][cols])
            ret.append(self.originalMatrix[row - 1][cols + 1])
            ret.append(self.originalMatrix[row][cols - 1])
            ret.append(self.originalMatrix[row][cols])
            ret.append(self.originalMatrix[row][cols + 1])
            ret.append(self.originalMatrix[row + 1][cols - 1])
            ret.append(self.originalMatrix[row + 1][cols])
            ret.append(self.originalMatrix[row + 1][cols + 1])

            random_numbers -= 1

        return np.asarray(ret, dtype=np.uint8)

    def submethod_random_method(self):
        """
        method change random amount of pixels according to set percentage
        @:return array of arrays - subset of input image matrix
        """
        random_matrix = random.sample(self.originalMatrix.flatten(), int(self.pixelCount))
        return np.asarray(random_matrix, dtype=np.uint8)

    def submethod_scale_nearest_neighbor(self):
        """
        method change scale of picture with nearest-neighbor interpolation
        @:return array of arrays - subset of input image matrix
        """
        if self.matrixHeight == self.matrixWidth:
            resize_matrix = cv2.resize(self.originalMatrix, None,
                                       fx=(math.sqrt(self.pixelCount) / self.matrixWidth),
                                       fy=(math.sqrt(self.pixelCount) / self.matrixHeight),
                                       interpolation=cv2.INTER_NEAREST)
        else:
            resize_matrix = cv2.resize(self.originalMatrix, None,
                                       fx=(math.sqrt(self.pixelCount / (self.matrixWidth * self.matrixHeight))),
                                       fy=(math.sqrt(self.pixelCount / (self.matrixWidth * self.matrixHeight))),
                                       interpolation=cv2.INTER_NEAREST)
        resize_array = resize_matrix.flatten()
        if self.pixelCount != resize_array.__len__():
            n = self.pixelCount
            cut_array = resize_array[:n]
            return np.asarray(cut_array, dtype=np.uint8)
        else:
            return np.asarray(resize_array, dtype=np.uint8)

    def submethod_scale_bilinear(self):
        """
        method change scale of picture with bilinear interpolation
        @:return array of arrays - subset of input image matrix
        """
        if self.matrixHeight == self.matrixWidth:
            resize_matrix = cv2.resize(self.originalMatrix, None,
                                       fx=(math.sqrt(self.pixelCount) / self.matrixWidth),
                                       fy=(math.sqrt(self.pixelCount) / self.matrixHeight),
                                       interpolation=cv2.INTER_LINEAR)
        else:
            resize_matrix = cv2.resize(self.originalMatrix, None,
                                       fx=(math.sqrt(self.pixelCount / (self.matrixWidth * self.matrixHeight))),
                                       fy=(math.sqrt(self.pixelCount / (self.matrixWidth * self.matrixHeight))),
                                       interpolation=cv2.INTER_LINEAR)
        resize_array = resize_matrix.flatten()
        if self.pixelCount != resize_array.__len__():
            n = self.pixelCount
            cut_array = resize_array[:n]
            return np.asarray(cut_array, dtype=np.uint8)
        else:
            return np.asarray(resize_array, dtype=np.uint8)

    def submethod_scale_bicubic(self):
        """
        method change scale of picture with bicubic interpolation
        @:return array of arrays - subset of input image matrix
        """
        if self.matrixHeight == self.matrixWidth:
            resize_matrix = cv2.resize(self.originalMatrix, None, fx=(math.sqrt(self.pixelCount) / self.matrixWidth),
                                       fy=(math.sqrt(self.pixelCount) / self.matrixHeight),
                                       interpolation=cv2.INTER_CUBIC)
        else:
            resize_matrix = cv2.resize(self.originalMatrix, None,
                                       fx=(math.sqrt(self.pixelCount / (self.matrixWidth * self.matrixHeight))),
                                       fy=(math.sqrt(self.pixelCount / (self.matrixWidth * self.matrixHeight))),
                                       interpolation=cv2.INTER_CUBIC)
        resize_array = resize_matrix.flatten()
        if self.pixelCount != resize_array.__len__():
            n = self.pixelCount
            cut_array = resize_array[:n]
            return np.asarray(cut_array, dtype=np.uint8)
        else:
            return np.asarray(resize_array, dtype=np.uint8)

    def submethod_section(self):
        ret = []

        # stred x a stred y -> v pulce matice
        x_centre = int(self.matrixWidth / 2)
        y_centre = int(self.matrixHeight / 2)

        # strana ctverce zaokrouhlena nahoru/2, protoze jdu pul nahoru a pul dolu
        a = int(math.ceil(math.ceil(math.sqrt(self.pixelCount)) / 2))

        x = x_centre - a
        i = 0
        while x < (x_centre + a) and i < self.pixelCount and x < self.matrixWidth:
            y = y_centre - a
            while y < (y_centre - 1 + a) and i < self.pixelCount - 1 and y < self.matrixHeight:
                ret.append(self.originalMatrix[y][x])
                y += 1
                i += 1
            x += 1

        return np.asarray(ret, dtype=np.uint8)

    def submethod_nth_element(self):
        resize_array = self.originalMatrix.flatten()
        array_len = resize_array.__len__()
        number = (self.matrixWidth * self.matrixHeight) / self.pixelCount # nezaokrouhlene cislo
        array_index = 0
        # i = 0
        ret = []
        while array_index < (array_len -1):
            # do ret pridam 1 cislo
            ret.append(resize_array[int(round(array_index))])  # az tady zaokrouhlim
            array_index = array_index + number # prictu  to nezaokrouhlene cislo
            # i = i + 1
        retx = np.asarray(ret[:int(self.pixelCount)], dtype=np.uint8)
        return retx

    def get_histogram(self, matrix, normalized=True, flat=True):
        start_time = time.time()
        hist = cv2.calcHist([matrix], [0], None, [256], [0, 256])
        stop_time = time.time()
        self.histTime = stop_time - start_time
        if normalized:
            if flat:
                size = matrix.__len__()  # size of matrix
            else:
                size = matrix.__len__() * matrix[0].__len__()  # size of matrix

            normalized_histogram = [value[0] / size for value in hist]
            # ^^ get just the numbers from calcHist array divided number of pixels to normalize
            return normalized_histogram
        else:
            return hist

    def plot_histogram(self, matrix, title=None, flat=True):
        normalized_histogram = self.get_histogram(matrix, normalized=True, flat=flat)
        plt.bar(range(normalized_histogram.__len__()), normalized_histogram, width=0)  # print the chart
        plt.xlabel('Greyscale', fontsize=12, color='black')
        plt.ylabel('Percentage', fontsize=12, color='black')
        if title is not None:
            plt.title(title)
        return None

    @staticmethod
    def nothing():
        print "wrong input"
        exit(2)

    def resolve_matrix(self, method_name):
        method_name = 'submethod_' + str(method_name)  # get name of the method
        method = getattr(self, method_name, lambda: self.nothing())  # based on the name set the method to call
        start_time = time.time()  # get the time before
        ret = method()  # run the method
        stop_time = time.time()  # get the time after
        self.methodTime = stop_time - start_time
        return ret

    def run(self, parameters):
        """
        get the method, run it and get the values for output
        """
        if not (parameters.__len__() == 3 or (parameters.__len__() == 4 and parameters[3] == "-p")):
            print "wrong parameters. please use python main.py <file> <method> <percentage>"
            return False
        plot = (parameters.__len__() == 4 and parameters[3] == "-p")  # plot the histograms??

        input_file = parameters[0]  # file to work with
        method = parameters[1]  # name of method to use
        self.percentage = int(parameters[2])  # how many of pixels to use

        self.originalMatrix = cv2.imread(input_file, cv2.IMREAD_GRAYSCALE)  # read the file in grayscale mode
        self.get_matrix_info()  # prepare info about the image
        matrix = self.resolve_matrix(method)  # get new matrix (apply the method)
        if plot:
            plt.subplot(211), self.plot_histogram(self.originalMatrix, "Original", False)
            plt.subplot(212), self.plot_histogram(matrix, "SubMethod", True)
            plt.show()
        else:
            self.originalHist = self.get_histogram(self.originalMatrix, flat=False)
            new_histogram = self.get_histogram(matrix, flat=True)

            # plot data in humanize form
            # print "diff: "+str(self.count_difference(new_histogram))
            # print "median: "+str(self.get_median(new_histogram))
            # print "histTime: "+str(self.histTime)
            # print "methodTime: "+str(self.methodTime)

            print "{0},{1},{2},{3}".format(str(self.count_difference(new_histogram)),
                                           str(self.get_median(new_histogram)), str(self.histTime),
                                           str(self.methodTime))
            # print data of differences, median and timing (to process data alter on)
        return True

    def get_differences(self, histogram_to_count):
        """
        get list of differences
        """
        return [math.fabs(item - self.originalHist[index]) for index, item in enumerate(histogram_to_count)]

    def count_difference(self, histogram_to_count):
        """
        sums the list of differences
        """
        return sum(self.get_differences(histogram_to_count))

    def get_median(self, histogram_to_count):
        """
        :return median
        """
        return np.median(self.get_differences(histogram_to_count))


if __name__ == "__main__":
    """
    usage : python main.py <input file> <method> <percentage>
    """
    histogram = Histogram()
    histogram.run(sys.argv[1:])
