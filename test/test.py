import os
import sys

# ZPO FIT VUT 2015/2016
# xpriby06 - Pribylova Katerina
# xobluk00 - Oblukova Alena
# xsemml01 - Semmler Jiri

def run(dataPath, method, percent=None):
    if dataPath == "all":
        dataPaths = ["lide", "priroda", "vzor"]
    else:
        dataPaths = [dataPath]

    for dataPath in dataPaths:
        # os.system("mkdir " + method+"/"+dataPath+" 2> /dev/null")
        os.system("echo '' >  " + method + "/" + dataPath + ".csv")

        data = os.listdir('../data/' + dataPath)
        # data = data[:2]  # take just few of the input files
        if percent is None:  # use all the percentages
            percents = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        elif percent == "skip":  # use just few of them
            percents = [30, 60, 80]
        else:
            percents = [percent]  # the percentage is set
        for percentItem in percents:
            # write data to csv files
            os.system("echo ',,,' >>  " + method + "/" + dataPath + ".csv")
            os.system("echo ',percentage " + str(percentItem) + ",,' >>  " + method + "/" + dataPath + ".csv")
            os.system("echo ',,,' >>  " + method + "/" + dataPath + ".csv")
            # os.system("echo ',,,' >> +method+" "+str(percentItem)+" >> "+method+"/"+dataPath+".csv")
            # os.system("echo ',,,' >> "+method+" "+str(percentItem)+" >> "+method+"/"+dataPath+".csv")

            for item in data:
                print "file : " + dataPath + " / " + item + " - " + method + " - " + str(percentItem)
                os.system("python ../src/main.py '../data/" + dataPath + "/" + item + "' " + method + " " + str(
                    percentItem) + " >> " + method + "/" + dataPath + ".csv")
    return True



if __name__ == "__main__":
    # data = ["patterning", "n_neighbourhood", "random_method", "scale_nearest_neighbor", "scale_bilinear",
            # "scale_bicubic", "section", "nth_element"]    
    data = [ "nth_element"]

    if sys.argv.__len__() < 3:
        print "wrong input. use python test.py <folder=all> <method=all> <percentage=all|skip|[10..90]>"
        exit()

    methodIN = sys.argv[2]
    folder = sys.argv[1]
    if methodIN == "all":  # do all the methods
        for method in data:
            os.system("mkdir " + method + " 2> /dev/null")
            print "############### metoda ##################  " + method

            if sys.argv.__len__() == 4:  # the percentage is set
                run(folder, method, sys.argv[3])  # [3] is percentage
            else:
                run(folder, method, percent=None)
    else:
        if sys.argv.__len__() == 4:
            run(folder, methodIN, sys.argv[3])  # [3] is percentage
        else:
            run(folder, methodIN)
